package com.nadisa.anan.servicebatch.model;

import java.util.Date;

import lombok.Data;
import lombok.ToString;
 
@Data
@ToString
public class Domain {
 
  private int id;
  private String memberType;
  private String firstName;
  private String lastName;
  private String id_mobile;
  private String email;
  private Date createDateTime;
  private String byStoretime;
  private Date lastPurchasedDate;
  private String poinBalance;
}