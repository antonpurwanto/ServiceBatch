package com.nadisa.anan.servicebatch.jobs;

import java.util.Date;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.nadisa.anan.servicebatch.model.Domain;
import com.nadisa.anan.servicebatch.scheduler.RunScheduler;
import com.nadisa.anan.servicebatch.writer.CustomWriter;
import com.nadisa.anan.servicebatch.writer.FileDeletingTasklet;


@Configuration
@EnableBatchProcessing
public class ScheduledDomainJob {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	@Autowired
	private DataSource dataSource;
	
	@Value("file:C:\\Users\\Anton\\eclipse-workspace\\service-batch\\src\\main\\resources\\csv\\")
	private Resource directory;

	@Bean
	public Job scheduledJob() {
		return jobBuilderFactory.get("scheduledJob").flow(step1()).next(step2()).end().build();
	}

	@Bean
	public JdbcBatchItemWriter<Domain> writer() {
		JdbcBatchItemWriter<Domain> writer = new JdbcBatchItemWriter<>();
		writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Domain>());
		writer.setSql("INSERT INTO domain (id, memberType,firstName,lastName,id_mobile,email,createDateTime,byStoretime,"
				+ "lastPurchasedDate,poinBalance) VALUES (:id, :memberType,"
				+ " :firstName, :lastName, :id_mobile, :email, :createDateTime, :byStoretime,:lastPurchasedDate,:poinBalance )");
		writer.setDataSource(dataSource);
		return writer;
	}
	

	@Bean
	public Step step1() {
		return stepBuilderFactory.get("step1").<Domain, Domain>chunk(10).reader(reader()).writer(writer()).build();
	}
	@Bean
	public Step step2() {
		return stepBuilderFactory.get("step2").tasklet(fileDeletingTasklet()).build();
	}

	@Bean
	public FlatFileItemReader<Domain> reader() {
		FlatFileItemReader<Domain> reader = new FlatFileItemReader<>();
		reader.setResource(new ClassPathResource("csv/domain-1-03-2017.csv"));
		reader.setLinesToSkip(1);
		reader.setLineMapper(new DefaultLineMapper<Domain>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer() {
					{
						setNames(new String[] { "id", "memberType","firstName","lastName",
								"id_mobile","email","createDateTime","byStoretime",
								"lastPurchasedDate","poinBalance" });										   
					}
				});
				setFieldSetMapper(new BeanWrapperFieldSetMapper<Domain>() {
					{
						setTargetType(Domain.class);
					}
				});
			}
		});
		return reader;
	}

	/*
	 * @Bean public CustomWriter writer() { CustomWriter writer = new
	 * CustomWriter(); return writer; }
	 */

	@Bean
	public RunScheduler scheduler() {
		RunScheduler scheduler = new RunScheduler();
		return scheduler;
	}
	
	@Bean
	public FileDeletingTasklet fileDeletingTasklet() {
		FileDeletingTasklet tasklet = new FileDeletingTasklet();
		tasklet.setDirectory(directory);
		return tasklet;
	}
}