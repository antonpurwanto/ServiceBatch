package com.nadisa.anan.servicebatch.writer;


import java.util.List;
import lombok.extern.slf4j.Slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import com.nadisa.anan.servicebatch.model.Domain;
 
@Slf4j
public class CustomWriter implements ItemWriter<Domain> {
	private static final Logger log = LoggerFactory.getLogger(CustomWriter.class);
  @Override
  public void write(List<? extends Domain> items) throws Exception {
    log.info("writer ....... " + items.size());
    for (Domain domain : items) {
      log.info(domain + "\n");
    }
  }
}
