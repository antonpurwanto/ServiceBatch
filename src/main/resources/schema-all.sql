DROP TABLE IF EXISTS domain;
 
CREATE TABLE domain  (
    id serial,
    memberType VARCHAR(30),
    firstName VARCHAR(30),
    lastName VARCHAR(30),
    id_mobile VARCHAR(30),
    email VARCHAR(50),
    createDateTime DATETIME, 
    byStoretime VARCHAR(25),
    lastPurchasedDate DATETIME,
    poinBalance  VARCHAR(20),
    UNIQUE(id)
);